# Services Models
Service Models is a package that regroup models used in uptimise project.

## Installation
Follow these few steps to use thi package on your workstation.
This is for workstation running in Linux or MacOS only.

### Requirements
Before following these steps, make sure to have installed:
* [Python3](https://phoenixnap.com/kb/how-to-install-python-3-ubuntu)
* [Pip3](https://linuxize.com/post/how-to-install-pip-on-ubuntu-18.04/)
* Virtual environment: `sudo apt install -y python3-venv` || `pip install virtualenv`
* [Git Installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Git Setup](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

Let's start now

### Two ways
### 1- Clone the project
```
git@gitlab.com:uptimise/tools/packages/bj/services-models.git
```
or
```
https://gitlab.com/uptimise/tools/package/bj/services-models.git
```

### Create a virtual environment to isolate our package dependencies locally
`python3 -m venv venv` || `virtualenv venv`

### Activate the virtual environment
`. venv/bin/activate`

### Move to the cloned project folder and install the requirements
```
pip3 install -r services-models
```


### 2- Use directly pip combined with git

```
pip install git+ssh//git@gitlab.com/uptimise/tools/package/bj/services-models.git.git
```
or
```
pip install git+https://gitlab.com/uptimise/tools/package/bj/services-models.git.git
```

## Usage
Nothing is simpler than this ):

**Happy Coding!**

