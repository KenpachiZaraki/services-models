from pathlib import Path

from setuptools import setup, find_packages

VERSION = '0.0.1'
DESCRIPTION = 'Uptimise Services Models Package for Benin version'
LONG_DESCRIPTION = 'Uptimise Services Models Package for Benin version'


def strip_comments(comment):
    return comment.split("#", 1)[0].strip()


def _pip_requirement(req, *root):
    if req.startswith("-r "):
        _, path = req.split()
        return reqs(*root, *path.split("/"))
    return [req]


def _reqs(*f):
    path = Path.cwd().joinpath(*f)
    with path.open() as fh:
        reqs = [strip_comments(line) for line in fh.readlines()]
        return [_pip_requirement(r, *f[:-1]) for r in reqs if r]


def reqs(*f):
    return [req for subreq in _reqs(*f) for req in subreq]


# Setting up
setup(
    # the name must match the folder name 'verysimplemodule'
    name="services-models",
    version=VERSION,
    author="Uptimise",
    author_email="core@erugis.fr",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    packages=find_packages(),
    include_package_data=True,
    install_requires=reqs("requirements.txt"),  # add any additional packages that
    # needs to be installed along with your package. Eg: 'caer'

    keywords=['python', 'services_models', 'backed'],
    classifiers=[]
)
