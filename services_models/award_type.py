import uuid

from django.db import models

from .timestamped import TimeStamped


class AwardType(TimeStamped, models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    value = models.CharField(max_length=100)
    label = models.CharField(max_length=100)
    is_automatic = models.BooleanField(default=False, null=False)

    class Meta:
        db_table = 'award_types'
        abstract = True

    def __str__(self):
        return str(self.value)
