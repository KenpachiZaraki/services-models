import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class RegulationDetail(SoftDeleteModel, models.Model):
    """"Deduction detail model"""
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    amount = models.CharField(max_length=100)
    date = models.DateField()
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    regulation = models.ForeignKey('Regulation', on_delete=models.CASCADE, related_name='regulation_details')
    is_last = models.BooleanField(null=True, blank=True, default=False)

    class Meta:
        db_table = 'regulation_details'
        abstract = True
