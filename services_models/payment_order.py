import uuid

from django.db import models

from .timestamped import TimeStamped


class PaymentOrder(TimeStamped, models.Model):
    # document information
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=255, null=True, blank=True)
    file = models.FileField(blank=True, null=True, max_length=500)
    date = models.DateField(default=None)

    class Meta:
        db_table = "payment_orders"
        ordering = ["-updated_at"]
        abstract = True
