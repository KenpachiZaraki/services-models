import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class Reimbursement(SoftDeleteModel, models.Model):
    # Method of payment
    CHECQUE = 1
    ESPECE = 2
    VIREMENT = 3
    PRELEVEMENT = 4

    REIMBURSEMENT_METHODS = [
        (CHECQUE, 'CHECQUE'),
        (ESPECE, 'ESPECE'),
        (VIREMENT, 'VIREMENT'),
        (PRELEVEMENT, "PRELEVEMENT"),
    ]
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    auto = models.BooleanField(default=True)
    amount = models.PositiveIntegerField()
    method = models.CharField(choices=REIMBURSEMENT_METHODS, max_length=50, default=PRELEVEMENT, null=True)
    date = models.DateField()
    proof = models.FileField(null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    salary_advance = models.ForeignKey("SalaryAdvance", on_delete=models.CASCADE, related_name='reimbursements')

    class Meta:
        db_table = "reimbursements"
        ordering = ['created_at']
        abstract = True

    def save(self, *args, **kwargs):
        company = kwargs.pop('company')
        self.proof.name = f'{company}/proofs/reimbursements/{self.proof}'
        super().save(*args, **kwargs)
