import uuid

from django.db import models


class AbsenceType(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    comments = models.TextField(blank=True, null=True, default=None)
    duration = models.IntegerField(null=True, blank=True, default=None)
    label = models.CharField(blank=True, null=True, max_length=255)
    value = models.CharField(blank=True, null=True, max_length=255)
    tag = models.CharField(max_length=255, blank=False, null=False)
    priority = models.IntegerField(null=False, blank=False)
    is_paid = models.BooleanField(blank=True, null=False, default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'absence_types'
        abstract = True
