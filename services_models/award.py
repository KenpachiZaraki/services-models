import uuid

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models  # , transaction
from django_softdelete.models import SoftDeleteModel
from extra_validator import FieldValidationMixin

from .timestamped import TimeStamped

PERCENTAGE_VALIDATOR = [MinValueValidator(0), MaxValueValidator(100)]


class Award(TimeStamped, FieldValidationMixin, SoftDeleteModel, models.Model):
    """
    Rewarding user
    """
    BASE_SALARY = 'BASE_SALARY'
    GROSS_SALARY = 'GROSS_SALARY'

    ONCE = 0
    MONTHLY = 1
    BIMONTHLY = 2
    QUARTERLY = 3
    BIANNUAL = 6
    ANNUAL = 12

    # Frequency of rewarding
    frequency_options = [
        (ONCE, "ONCE"),
        (MONTHLY, "MONTHLY"),
        (BIMONTHLY, "BIMONTHLY"),
        (QUARTERLY, "QUARTERLY"),
        (BIANNUAL, "BIANNUAL"),
        (ANNUAL, "ANNUAL"),
    ]

    REQUIRED_TOGGLE_FIELDS = [
        ['award_type', 'other_type_label']  # Require only one of the following fields.
    ]

    CALCUL_BASE = [
        (GROSS_SALARY, "GROSS_SALARY"),
        (BASE_SALARY, "BASE_SALARY")
    ]

    # require percentage and amount_calcul basis when it's not a fixed amount
    CONDITIONAL_REQUIRED_FIELDS = [
        (
            lambda instance: instance.amount is None, ['percentage', 'amount_calcul_basis']
        )
    ]

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    user = models.ForeignKey('User', on_delete=models.CASCADE, null=False, related_name="users_awards")
    award_type = models.ForeignKey('AwardType', on_delete=models.CASCADE, null=True, related_name="awards_types_award")
    amount = models.PositiveIntegerField(blank=True, null=True)
    percentage = models.DecimalField(max_digits=3, decimal_places=0, null=True, blank=True,
                                     validators=PERCENTAGE_VALIDATOR)
    frequency = models.IntegerField(choices=frequency_options)
    amount_calcul_basis = models.CharField(max_length=100, blank=True, null=True, choices=CALCUL_BASE,
                                           default=None)

    include_in_gross_salary = models.BooleanField(default=False)
    other_type_label = models.CharField(max_length=100, null=True, blank=False)

    start_date = models.DateField(default=None)
    end_date = models.DateField(null=True, blank=True)

    expired = models.BooleanField(default=False)

    class Meta:
        db_table = 'awards'
        # unique_together = ('user', 'award_type', 'start_date'),
        abstract = True
