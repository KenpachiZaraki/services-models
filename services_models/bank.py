import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class Bank(SoftDeleteModel, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=100, blank=False, null=True)
    agency = models.CharField(max_length=100, blank=True, null=True)
    iban = models.CharField(max_length=100, blank=False, null=True)
    swift = models.CharField(max_length=100, blank=False, null=True)
    manager_first_name = models.CharField(max_length=100, blank=True, null=True)
    manager_last_name = models.CharField(max_length=100, blank=True, null=True)
    manager_email = models.EmailField(blank=False, null=False, default='', unique=True, )
    manager_phone = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "banks"
        abstract = True
