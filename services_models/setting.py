import uuid

from django.db import models
from django.utils.text import slugify
from django_softdelete.models import SoftDeleteModel


class Setting(SoftDeleteModel, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(max_length=100, blank=True, null=True, unique=True)
    slug = models.SlugField(max_length=100, blank=True, null=True)
    content = models.JSONField(default=dict)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "settings"
        abstract = True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)
