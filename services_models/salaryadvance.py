import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class SalaryAdvance(SoftDeleteModel, models.Model):
    """Salary advance model"""

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    label = models.CharField(max_length=255, blank=True, null=True)
    amount = models.PositiveIntegerField()
    repayment_period = models.SmallIntegerField()  # repayment period in month
    monthly_repayment = models.IntegerField()  # amount to be repaid monthly
    start_payment = models.DateField()  # start of the payment
    start_repayment = models.DateField()  # start of the repayment
    is_paid = models.BooleanField(default=False)  # True if user reimburse salary advance
    is_displayed = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='salary_advances')

    class Meta:
        db_table = 'advances'
        ordering = ['created_at']
        abstract = True
