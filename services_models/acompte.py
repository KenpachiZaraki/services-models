import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class Acompte(SoftDeleteModel, models.Model):
    """Acompte model"""

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    amount = models.PositiveIntegerField()
    payment_date = models.DateField()  # start of the payment
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='acomptes')

    class Meta:
        db_table = 'acomptes'
        ordering = ['created_at']
        abstract = True
