import uuid
from typing import List

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.files.storage import default_storage
from django.db import models
from django.db.models import IntegerField, CharField, F
from django.db.models.functions import Cast
from django_softdelete.models import SoftDeleteModel

from .timestamped import TimeStamped


class Document(SoftDeleteModel, TimeStamped):
    PAYSHEET = "paysheet"
    PAYMENT_ORDER = "paymentorder"
    PAYROLL_STATEMENT = "payrollstatement"

    DOCUMENTS_TYPES = [
        (PAYSHEET, PAYSHEET),
        (PAYMENT_ORDER, PAYMENT_ORDER),
        (PAYROLL_STATEMENT, PAYROLL_STATEMENT)
    ]

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=255, null=True, blank=True)
    is_file = models.BooleanField(default=True)
    is_public = models.BooleanField(default=False)
    file = models.FileField(blank=True, null=True, max_length=500)
    size = models.CharField(max_length=100, default='0')
    date = models.DateField(null=True, blank=True)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='sub_folders')
    parents = models.JSONField(default=list)
    document_type = models.CharField(max_length=255, choices=DOCUMENTS_TYPES, default=None, null=True, blank=True)
    creator = models.ForeignKey('User', related_name='documents_created', on_delete=models.CASCADE, blank=True,
                                null=True)
    users = models.ManyToManyField('User', through='SharedWith', related_name='documents', blank=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    object_id = models.UUIDField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        db_table = "documents"
        ordering = ("is_file",)
        abstract = True

    def save(self, *args, **kwargs):
        is_new = self._state.adding
        if is_new:
            self.file.storage = default_storage
            self.file.name = self.gen_path()
            if self.is_file:
                update_parents_sizes(instance=self)
        super().save(*args, **kwargs)

    def gen_path(self):
        return "/".join([p['name'] for p in list(self.parents)] + ['']) + f'{self.name}'


class SharedWith(SoftDeleteModel, TimeStamped):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    document = models.ForeignKey('Document', on_delete=models.CASCADE)
    is_owned = models.BooleanField(default=False)
    permissions = models.JSONField(default=dict)

    class Meta:
        db_table = 'shared_with'
        abstract = True


def update_parents_sizes(instance, rm=1, *args, **kwargs):
    if not (size := kwargs.get('size', None)):
        size = rm * int(instance.size)
    Document.objects.filter(pk__in=[x.get("id") for x in instance.parents]) \
        .annotate(size_as_int=Cast('size', IntegerField())) \
        .update(size=Cast(F('size_as_int') + size, CharField()))


def group_documents_by_date(documents: List[Document]):
    documents_by_date = {}

    for document in documents:
        if document.date not in documents_by_date:
            documents_by_date[document.date] = []
        documents_by_date[document.date].append(document)

    return documents_by_date
