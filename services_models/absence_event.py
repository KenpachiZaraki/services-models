import uuid

from django.db import models


class AbsenceEvent(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    comments = models.TextField(blank=True, null=True, default=None)
    duration = models.IntegerField(null=False, blank=False, default=None)
    label = models.CharField(blank=True, null=True, max_length=255)
    value = models.CharField(blank=True, null=True, max_length=255)
    type = models.ForeignKey('AbsenceType', related_name='events', related_query_name='event', on_delete=models.CASCADE,
                             blank=False, null=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'absence_events'
        abstract = True
