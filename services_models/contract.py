import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class Contract(SoftDeleteModel, models.Model):
    """
    Salarié / apprenti/ stagiaire / prestataire
    SALARIE in uppercase
    """

    EMPLOYEE = "SALARIE"
    APPRENTICE = "APPRENTI"
    INTERN = "STAGIAIRE"
    FREELANCER = "PRESTATAIRE"

    STATUS_CHOICES = [
        (EMPLOYEE, 'SALARIE'),
        (APPRENTICE, 'APPRENTI'),
        (INTERN, 'STAGIAIRE'),
        (FREELANCER, 'PRESTATAIRE'),

    ]

    CONTRACT_END_REASON = [
        ("LICENCIEMENT", 'LICENCIEMENT'),
        ("ACCORD DES PARTIES", "ACCORD DES PARTIES"),
        ("DEMISSION", "DEMISSION"),
        ("FAUTE LOURDE", "FAUTE LOURDE"),
        ("FIN DE LA DUREE DU CONTRAT", "FIN DE LA DUREE DU CONTRAT"),
        ("RETRAITE", "RETRAITE"),
        ("DECES", "DECES")
    ]

    KIND_CHOICES = [("CDD", 'CDD'), ("CDI", 'CDI')]
    TIME_MEASURE = [('MOD 1', 'MOD 1'), ('MOD 2', 'MOD 2'), ('MOD 3', 'MOD 3')]
    WORK_TIME_BY = [('WEEK', 'WEEK'), ('MONTH', 'MONTH')]
    SALARY_BY = [('HOUR', 'HOUR'), ('MONTH', 'MONTH'), ('YEAR', 'YEAR')]
    SALARY_FROM = [('GROSS', 'GROSS'), ('NET', 'NET')]

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    user_status = models.CharField(max_length=100, choices=STATUS_CHOICES)
    kind = models.CharField(max_length=100, choices=KIND_CHOICES)
    particular_system = models.CharField(max_length=100)
    # Classification
    """
        Agents d'exécutions; Agents de maitrises; cadre et assimilés
    """
    # ifu = models.BigIntegerField(null=True, blank=True, unique=True)
    classification = models.CharField(max_length=100)
    particular_appliance = models.CharField(max_length=100, null=True)
    category = models.CharField(max_length=100, blank=True, null=True)
    job_designation = models.CharField(max_length=100)
    salary = models.BigIntegerField(default=0, null=False, blank=False)

    # team to be added
    start = models.DateField(default=None, null=True, blank=True)
    end = models.DateField(default=None, null=True, blank=True)
    trial_start = models.DateTimeField(default=None, null=True, blank=True)
    trial_end = models.DateTimeField(default=None, null=True, blank=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE)

    time_measure = models.CharField(max_length=100, choices=TIME_MEASURE, null=True)
    work_time_by = models.CharField(max_length=100, choices=WORK_TIME_BY, null=True)
    salary_by = models.CharField(max_length=100, choices=SALARY_BY, null=True)

    work_time = models.IntegerField(null=True, default=0)
    is_active = models.BooleanField(default=True)
    # recovery = {previous_acquired: int, previous_taken: int, acquired: int, taken: int, }
    recovery = models.JSONField(default=dict)
    # olds_salaries = {...{year_i: val_i}}
    olds_salaries = models.JSONField(default=dict)

    # end contract
    reason = models.CharField(max_length=100, choices=CONTRACT_END_REASON, null=True)
    is_ended = models.BooleanField(default=False)
    out_date = models.DateField(default=None, null=True, blank=True)
    accord_date = models.DateField(default=None, null=True, blank=True)
    ended_at = models.DateField(default=None, null=True, blank=True)
    proof = models.FileField(null=True, blank=True)
    holidays_compensation = models.BigIntegerField(null=True, default=0)
    preavi_indemnity = models.BigIntegerField(null=True, default=0)
    severance_pay = models.BigIntegerField(null=True, default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'contracts'
        abstract = True
