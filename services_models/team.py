import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel

from .timestamped import TimeStamped


class Team(TimeStamped, SoftDeleteModel, models.Model):
    """Team model"""

    class PaletColor(models.TextChoices):
        RED = 'RED', 'Rouge'
        BLUE = 'BLUE', 'Bleu'
        GREEN = 'GREEN', 'Vert'
        YELLOW = 'YELLOW', 'Jaune'
        PURPLE = 'PURPLE', 'Violet'
        PINK = 'PINK', 'Rose'
        ORANGE = 'ORANGE', 'Orange'
        BROWN = 'BROWN', 'Brun'
        BLACK = 'BLACK', 'Noir'
        WHITE = 'WHITE', 'Blanc'
        GRAY = 'GRAY', 'Gris'

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    name = models.TextField(blank=False, null=False)
    code = models.TextField(blank=False, null=False, unique=True, default='')
    cover = models.ImageField(default=None, null=True, blank=True)
    color = models.TextField(max_length=10, choices=PaletColor.choices, default=PaletColor.WHITE)
    is_department = models.BooleanField(default=False)
    description = models.TextField(blank=True, null=True)
    manager = models.ForeignKey('User', on_delete=models.CASCADE, blank=True, null=True,
                                related_name="team_manager")
    members = models.ManyToManyField("User", through='TeamUser', related_name='team_members', blank=True)
    parent = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='sub_team')

    class Meta:
        db_table = 'teams'
        ordering = ['created_at']
        abstract = True


class TeamUser(SoftDeleteModel, models.Model):
    """Association table between team and user"""
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    team = models.ForeignKey('Team', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'team_user'
        ordering = ['created_at']
        abstract = True
