import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class Job(SoftDeleteModel, models.Model):
    PENDING = "pending"
    DONE = "done"

    JOB_STATUS = [
        (PENDING, 'pending'),
        (DONE, 'done'),
    ]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    tag = models.CharField(max_length=100, blank=False, null=False)
    status = models.CharField(max_length=100, blank=True, null=True, choices=JOB_STATUS)
    date = models.DateField(default=None)
    services_status = models.JSONField(default=dict)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "jobs"
        ordering = ['created_at']
        abstract = True
