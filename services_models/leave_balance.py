import uuid

from django.db import models

from .timestamped import TimeStamped


class LeaveBalance(TimeStamped, models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    year = models.PositiveIntegerField()
    curr_balance = models.CharField(max_length=255, null=True, blank=True, default=0)
    taken = models.CharField(max_length=255, null=True, blank=True, default=0)

    user = models.ForeignKey("User", on_delete=models.CASCADE, blank=False, null=False,
                             related_name="leave_balances")

    class Meta:
        db_table = "leave_balances"
        ordering = ["-updated_at"]
        unique_together = (('user', 'year'),)
        abstract = True

