import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel


class Regulation(SoftDeleteModel, models.Model):
    """ Regulation frequency"""
    ONCE = 0
    MONTHLY = 1
    BIMONTHLY = 2
    QUARTELY = 3
    BIANNUAL = 6
    ANNUAL = 12

    FREQUENCY = [
        (ONCE, "ONCE"),
        (MONTHLY, "BIANNUAL"),
        (BIMONTHLY, "BIMONTHLY"),
        (QUARTELY, "QUARTELY"),
        (BIANNUAL, "BIANNUAL"),
        (ANNUAL, "ANNUAL"),
    ]

    """Regulation type"""
    DEDUCTION = "DEDUCTION"
    INDEMNITY = "INDEMNITY"

    TYPE = [
        (DEDUCTION, "DEDUCTION"),
        (INDEMNITY, "INDEMNITY")
    ]

    """"Deduction model"""
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    reason = models.CharField(max_length=1000)
    amount = models.CharField(max_length=100)
    type = models.CharField(max_length=100, choices=TYPE, null=True)
    start_payment = models.DateField()
    end_payment = models.DateField(default=None, null=True, blank=True)
    frequency = models.IntegerField(choices=FREQUENCY, null=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    expired = models.BooleanField(default=False)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='regulations')

    class Meta:
        db_table = 'regulations'
        abstract = True
