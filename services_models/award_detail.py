import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel

from .timestamped import TimeStamped


class AwardDetail(TimeStamped, SoftDeleteModel, models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    date = models.DateField(default=None)
    amount = models.CharField(max_length=100, null=False, blank=False)
    award = models.ForeignKey("Award", on_delete=models.CASCADE, blank=False, null=False, related_name="award_details")
    is_last = models.BooleanField(null=True, blank=True, default=False)

    class Meta:
        db_table = "award_details"
        ordering = ["-updated_at"]
        abstract = True
