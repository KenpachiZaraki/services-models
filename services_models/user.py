import uuid

from django.core.validators import EmailValidator
from django.db import models
from django_softdelete.models import SoftDeleteModel


class User(SoftDeleteModel, models.Model):
    """
     Class for user
     """
    TYPES_SEX = [
        ('M', 'M'),
        ('F', 'F'),
    ]

    # Roles Choices
    OWNER = "OWNER"
    ADMIN = "ADMIN"
    ACCOUNTANT = "ACCOUNTANT"
    MANAGER = "MANAGER"
    COLLABO = "COLLABO"

    # Marital status
    DIVORCE = "DIVORCE"
    MARIE = "MARIE"
    CELIBATAIRE = "CELIBATAIRE"
    VEUF = "VEUF"

    # Invitation status
    NOT_SENT = "NOT_SENT"
    SENT = "SENT"
    CANCELLED = "CANCELLED"
    ACCEPTED = "ACCEPTED"

    MARITAL_STATUS = [
        (DIVORCE, DIVORCE),
        (MARIE, MARIE),
        (CELIBATAIRE, CELIBATAIRE),
        (VEUF, VEUF),

    ]

    INVITATION_STATUS = [
        (NOT_SENT, NOT_SENT),
        (SENT, SENT),
        (CANCELLED, CANCELLED),
        (ACCEPTED, ACCEPTED)

    ]

    ROLES = [
        (OWNER, OWNER),
        (ADMIN, ADMIN),
        (ACCOUNTANT, ACCOUNTANT),
        (MANAGER, MANAGER),
        (COLLABO, COLLABO),
    ]

    """
    personal identity 
    """
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    last_name = models.CharField(max_length=100, blank=True, null=True, default='')
    first_name = models.CharField(max_length=100, blank=True, null=True, default='')
    usual_name = models.CharField(max_length=100, blank=True, null=True, default='')
    username = models.CharField(max_length=12, blank=False, null=False, default='', unique=True)
    email = models.EmailField(blank=False, null=False, default='', unique=True, validators=[EmailValidator])
    mobile_phone = models.CharField(max_length=255, blank=True)
    mobile_money = models.CharField(max_length=255, blank=True)
    company_name = models.CharField(max_length=100, blank=True, null=True)
    matricule = models.CharField(max_length=12, blank=True, null=True, default='', unique=True)

    """
    Profile information
    """
    # benin personal identification number
    nip = models.CharField(max_length=255, blank=True, null=True, unique=True, default=None)
    ifu = models.CharField(max_length=13, null=True, blank=True, unique=True)

    sex = models.CharField(max_length=1, choices=TYPES_SEX)
    birthday = models.DateField(null=True, blank=True)
    birthplace = models.CharField(max_length=100, null=True, blank=True)
    birthcountry = models.CharField(max_length=100, null=True, blank=True)
    nationality = models.CharField(max_length=100, null=True, blank=True)

    is_old = models.BooleanField(default=False)
    location = models.CharField(max_length=255, blank=True)
    department = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)
    town = models.CharField(max_length=255, blank=True)

    photo = models.ImageField(upload_to='', blank=True, max_length=500)

    # Social security
    cnss_number = models.CharField(max_length=255, null=True, blank=True)

    # Personal situation
    handicap_id_card_number = models.CharField(max_length=255, null=True, blank=True)
    handicap_recognition_date = models.DateField(null=True, blank=True)
    graduation = models.CharField(max_length=255, blank=True)

    work_license_id = models.CharField(max_length=255, null=True, blank=True, default=None)
    work_permit_date = models.DateField(max_length=255, null=True, blank=True, default=None)

    # Situation familiale
    children_charge = models.IntegerField(null=True, default=0)
    marital_status = models.CharField(max_length=100, choices=MARITAL_STATUS, null=True, blank=False)
    emergency_contacts = models.JSONField(null=True, blank=True)

    """
    Bank account information's
    """
    bank = models.CharField(max_length=100, blank=False, null=True)
    bank_account_details = models.JSONField(default='', null=True)
    payment_method = models.CharField(max_length=100, blank=True)

    # enrollment status
    enrollment_status = models.JSONField(default=list, null=True, blank=True)
    additional_infos = models.JSONField(default=dict, null=True, blank=True)
    seniority_duration = models.PositiveIntegerField(blank=True, null=True, default=0)
    is_active = models.BooleanField(default=False)

    is_primary = models.BooleanField(default=False)
    is_collabo = models.BooleanField(default=True)
    is_member = models.BooleanField(default=False)
    is_enabled = models.BooleanField(default=False)

    role = models.CharField(max_length=255, blank=True, null=True, choices=ROLES)
    invitation_status = models.CharField(max_length=100, choices=INVITATION_STATUS, default=NOT_SENT)

    first_contract = models.DateField(default=None, null=True, blank=True)

    # readonly fields
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "users"
        abstract = True
