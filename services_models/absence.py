import uuid

from django.db import models
from django_softdelete.models import SoftDeleteModel
from extra_validator import FieldValidationMixin


class Absence(FieldValidationMixin, SoftDeleteModel, models.Model):
    CANCELED_PENDING = 'CANCELED_PENDING'
    PENDING = "PENDING"
    VALIDATED = "VALIDATED"
    REFUSED = "REFUSED"
    CANCELED = "CANCELED"

    ABSENCE_STATUS = [
        ('VALIDATED', 'VALIDATED'),
        ('CANCELED', 'CANCELED'),
        ('CANCELED_PENDING', 'CANCELED_PENDING'),
        ('PENDING', 'PENDING'),
        ('REFUSED', 'REFUSED')
    ]

    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    comments = models.TextField(blank=True, null=True, default=None)
    duration = models.CharField(null=False, blank=False, default=None, max_length=255)
    start_date = models.DateField(blank=False, null=False, default=None)
    start_on_half = models.BooleanField(default=False)
    end_date = models.DateField(blank=True, null=True)
    end_on_half = models.BooleanField(default=False)
    status = models.CharField(blank=True, null=True, max_length=255, default=PENDING, choices=ABSENCE_STATUS)
    motif = models.CharField(blank=True, null=True, max_length=255, default=None)

    event = models.ForeignKey('AbsenceEvent', on_delete=models.CASCADE, blank=True, null=True)
    type = models.ForeignKey('AbsenceType', on_delete=models.CASCADE, blank=False, null=False)
    applicant = models.ForeignKey('User', on_delete=models.CASCADE, blank=False, null=False, related_name="absences")
    assignor = models.ForeignKey('User', on_delete=models.CASCADE, blank=True, null=True,
                                 related_name="assigned_absences")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    CONDITIONAL_REQUIRED_FIELDS = [
        (
            lambda instance: instance.event, ['type'],
        ),
    ]

    class Meta:
        db_table = 'absences'
        ordering = ['-updated_at']
        abstract = True
