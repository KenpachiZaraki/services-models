import uuid

from django.db import models

from .timestamped import TimeStamped


class Paysheet(TimeStamped, models.Model):
    id = models.UUIDField(primary_key=True, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=255, null=True, blank=True)
    content = models.JSONField(default=dict)
    code = models.CharField(max_length=6, blank=True, null=True, default=None)
    date = models.DateField(default=None)

    # user informations
    user = models.ForeignKey('User', on_delete=models.CASCADE, blank=False, null=False, default=None,
                             related_name="paysheet")

    class Meta:
        db_table = "paysheets"
        ordering = ["-updated_at"]
        abstract = True
